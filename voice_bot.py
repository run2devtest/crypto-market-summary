from speech_client import send_voice_msg

from logger import GetLogger

log = GetLogger()


def convert_change_for_speech(value):
    if value > 0:
        return 'is up {:.3f} %'.format(value)
    elif value < 0:
        return 'is down negative{:.3f} %'.format(abs(value))
    else:
        return 'has not changed'


def vocal_market_summary(info):
    text = 'In the last {}'.format(info['time_chg'])
    text += 'the crypto market cap {}, '.format(
        convert_change_for_speech(info['mcap_chg']))
    text += 'bitcoin market cap {}, '.format(
        convert_change_for_speech(info['bmcap_chg']))

    text += 'volume {}, '.format(convert_change_for_speech(info['vol_chg']))
    text += 'bitcoin dominance {}, '.format(
        convert_change_for_speech(info['bmcap_chg']))
    send_voice_msg(text)


def vocal_market_status(data):
    text = data

    try:
        send_voice_msg(data)

    except ConnectionRefusedError:
        log.info('Speech Server Down')


def test():
    vocal_market_status('Testing Market Summary Voice Alert')

if __name__ == '__main__':
    test()
