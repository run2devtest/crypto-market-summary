import telegram
from telegram.error import TimedOut

from dhooks.discord_hooks import Webhook
from gettoken import GetToken
from logger import GetLogger

log = GetLogger()
channel = '@cyrpto_market_stats'


def htmlFixedMessage(message):
    if message:
        return '<pre>{}</pre>'.format(message)
    return '<pre>No Data Avaliable</pre>'


def sendMessage(message):
    try:
        message = htmlFixedMessage(message)
        bot = telegram.Bot(GetToken('TELEGRAM_TOKEN'))
        bot.send_message(chat_id=channel, text=message,
                         parse_mode=telegram.ParseMode.HTML)
    except TimedOut:
        log.info('Message Timedout!'.upper())


def sendDiscord(message):
    TOKEN = GetToken('DISCORD_WEBHOOK_TOKEN')
    msg = Webhook(TOKEN, msg='```{}```'.format(message))
    msg.post()
