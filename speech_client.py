import socket

from logger import GetLogger

log = GetLogger()


def send_voice_msg(message):
    try:
        # create an ipv4 (AF_INET) socket object using the tcp protocol
        # (SOCK_STREAM)
        client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        # connect the client
        client.connect(('0.0.0.0', 6666))

        # send some data (in this case a HTTP GET request)
        client.send(message.encode())

        # receive the response data (4096 is recommended buffer size)
        response = client.recv(4096)

        if response.decode() == 'ACK!':
            log.info('SERVER ACK RECEIVED.')
        else:
            log.info('NO ACK RECIEVED MESSAGE NOT SENT.')

    except ConnectionRefusedError:
        log.info('Speech Server not running.'.upper())

if __name__ == '__main__':
    send_voice_msg('testing client')
