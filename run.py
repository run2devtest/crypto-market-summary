import atexit
from coinmarketcap import Market
import datetime
from datetime import datetime, timedelta, date
from pandas import DataFrame, to_pickle, read_pickle, Series, concat, pivot
import schedule
import time
from time import sleep

import message_bot as bot

from logger import GetLogger
from voice_bot import vocal_market_summary, vocal_market_status

log = GetLogger()


def gettimezone():
    return time.tzname[time.daylight]


if gettimezone() == 'CDT':
    USOPEN = datetime.strptime('08:30', '%H:%M')
    USCLOSE = datetime.strptime('15:00', '%H:%M')
    ASIANOPEN = datetime.strptime('18:00', '%H:%M')
    ASIANCLOSE = datetime.strptime('03:00', '%H:%M')
    EUROPEOPEN = datetime.strptime('02:00', '%H:%M')
    EUROPECLOSE = datetime.strptime('10:35', '%H:%M')

    FOUR_HOUR_START = datetime.strptime('03:00', '%H:%M')

elif gettimezone() == 'UTC':
    USOPEN = datetime.strptime('13:30', '%H:%M')
    USCLOSE = datetime.strptime('20:00', '%H:%M')
    ASIANOPEN = datetime.strptime('00:00', '%H:%M')
    ASIANCLOSE = datetime.strptime('09:00', '%H:%M')
    EUROPEOPEN = datetime.strptime('07:00', '%H:%M')
    EUROPECLOSE = datetime.strptime('15:35', '%H:%M')

    FOUR_HOUR_START = datetime.strptime('00:00', '%H:%M')

else:
    log.info('Timezone not implemented.')
    raise Exception('Timezone no implemented.')


ONEHOURPRE = timedelta(minutes=60)
THRITYMINPRE = timedelta(minutes=30)
FOUR_HOUR_CANDLES = []


def fixtime(x):
    return str(x.time().strftime('%H:%M'))


def dt(u): return datetime.utcfromtimestamp(u)


def get_cmc_data():
    x = Market()
    stats = x.stats()
    btc = x.ticker('Bitcoin')

    date = dt(stats['last_updated'])
    total_mcap = int(stats['total_market_cap_usd'])
    total_vol = int(stats['total_24h_volume_usd'])
    btc_dominance = float(stats['bitcoin_percentage_of_market_cap'])

    btc_mcap = int(btc[0]['market_cap_usd'])

    data = {'vol': total_vol,
            'mcap': total_mcap,
            'bmcap': btc_mcap,
            'btc_dom': btc_dominance,
            'mcap_xbtc': (total_mcap - btc_mcap)}

    return DataFrame(data, index=[date], columns=['vol', 'mcap', 'bmcap', 'btc_dom', 'mcap_xbtc'])


def get_cmc_stats():
    try:
        last_pickle = read_pickle('current.pickle')
        current_data = get_cmc_data()
        current_data.to_pickle('current.pickle')

        df = DataFrame(concat([last_pickle, current_data]))
        start_time = list(df.index)[0]
        end_time = list(df.index)[1]
        change = df.pct_change().dropna()
        change.rename(columns={'vol': 'vol_chg',
                               'mcap': 'mcap_chg', 'bmcap': 'bmcap_chg', 'btc_dom': 'btc_dom_chg', 'mcap_xbtc': 'mcap_xbtc_chg'}, inplace=True)

        time_change = end_time - start_time

        if int(str(time_change).split(' ')[0]):
            change['time_chg'] = str(time_change)
        else:
            change['time_chg'] = str(time_change).split(' ')[2]

        return current_data.join(change).iloc[0].to_dict()

    except FileNotFoundError:
        new = get_cmc_data()
        new.to_pickle('current.pickle')

        log.info('FileDoesntExist')


summary_msg = """Market Cap:          ${:,}
Volume:              ${:,}
BTC Dominance:       {:.3f}%

M-Cap Excluding BTC: ${:,}
BTC Market Cap:      ${:,}

PERCENT CHANGE SINCE LAST UPDATE:
Time Since Update:   {}
Market Cap:          {:.3f}%
Volume:              {:.3f}%
BTC Dominance:       {:.3f}%
M-Cap Excluding BTC: {:.3f}%
BTC Marketcap:       {:.3f}%
"""


def market_summary():
    data = get_cmc_stats()
    log.info(summary_msg.format(data['mcap'], data['vol'], data['btc_dom'], data['mcap_xbtc'], data['bmcap'], data[
        'time_chg'], data['mcap_chg'], data['vol_chg'], data['btc_dom_chg'], data['mcap_xbtc_chg'], data['bmcap_chg']))
    bot.sendMessage(summary_msg.format(data['mcap'], data['vol'], data['btc_dom'], data['mcap_xbtc'], data['bmcap'], data[
        'time_chg'], data['mcap_chg'], data['vol_chg'], data['btc_dom_chg'], data['mcap_xbtc_chg'], data['bmcap_chg']))
    bot.sendDiscord(summary_msg.format(data['mcap'], data['vol'], data['btc_dom'], data['mcap_xbtc'], data['bmcap'], data[
        'time_chg'], data['mcap_chg'], data['vol_chg'], data['btc_dom_chg'], data['mcap_xbtc_chg'], data['bmcap_chg']))
    vocal_market_summary(data)
    # add a send market_summary


def market_alert(message):
    log.info(message)
    bot.sendMessage(message)
    bot.sendDiscord(message)
    vocal_market_status(message)


def market_reminder():
    msg = "Get prepared.\nDon't fomo.\nDon't buy tops.\nWait for pullbacks, breakouts, and dips.\nSet good stoplosses.\nNever go all in.\n"
    msg += 'FOMO DRIVEN DEVELOPEMENT.'
    market_alert(msg)


def mid_month_trasition():
    today = date.today().day
    if today == 1:
        msg = "This is the beginning of the month.\n"
    elif today == 15:
        msg = "This is the middle of the month.\n"
    else:
        return None
    msg += 'Expect a significant change in marekt direction.\n'
    msg += 'Or expect significant movement in the current market direction.'
    market_alert(msg)


#/////////////////////////////////////////////


def us_1hour_preopen():
    msg = 'U S Markets opening in one hour.'
    market_alert(msg)
    market_reminder()


def us_30min_preopen():
    msg = 'U S Markets opening in 30 minutes.'
    market_alert(msg)
    market_summary()


def us_markets_open():
    msg = 'U S Markets open'
    market_alert(msg)
    mid_month_trasition()

# CLOSE


def us_1hour_preclose():
    msg = 'U S Markets closing in one hour.'
    market_alert(msg)


def us_30min_preclose():
    msg = 'U S Markets closing in 30 minutes.'
    market_alert(msg)


def us_markets_close():
    msg = 'U S Markets closed.'
    market_alert(msg)

#//////////////////////////////////////////////////


def asian_1hour_preopen():
    msg = 'Asian Markets opening in one hour.'
    market_alert(msg)
    market_reminder()


def asian_30min_preopen():
    msg = 'Asian Markets opening in 30 minutes.'
    market_alert(msg)
    market_summary()


def asian_markets_open():
    msg = 'Asian Markets open'
    market_alert(msg)
    mid_month_trasition()
# CLOSE


def asian_1hour_preclose():
    msg = 'Asian Markets closing in one hour.'
    market_alert(msg)


def asian_30min_preclose():
    msg = 'Asian Markets closing in 30 minutes.'
    market_alert(msg)


def asian_markets_close():
    msg = 'Asian Markets closed.'
    market_alert(msg)


#?//////////////////////////////////////////////////


def european_1hour_preopen():
    msg = 'European Markets opening in one hour.'
    market_alert(msg)
    market_reminder()


def european_30min_preopen():
    msg = 'European Markets opening in 30 minutes.'
    market_alert(msg)
    market_summary()


def european_markets_open():
    msg = 'European Markets open'
    market_alert(msg)
    mid_month_trasition()
# CLOSE


def european_1hour_preclose():
    msg = 'European Markets closing in one hour.'
    market_alert(msg)


def european_30min_preclose():
    msg = 'European Markets closing in 30 minutes.'
    market_alert(msg)


def european_markets_close():
    msg = 'European Markets closed.'
    market_alert(msg)


def four_hour_close():
    # 22, 18, 14, 10, 6, 2
    msg = 'Four Hour Candle closing in 15 minutes.'
    market_alert(msg)


@atexit.register
def goodbye():
    market_alert('Market Summary BOT HALTED')


if __name__ == '__main__':
    log.info('Time Zone: {}'.format(gettimezone()))
    log.info('Running market summary bot...')
    market_alert('Market Summary BOT STARTED')
    mid_month_trasition()
    # market_summary()
    # usopen
    schedule.every().day.at(fixtime((USOPEN - ONEHOURPRE))).do(us_1hour_preopen)
    schedule.every().day.at(fixtime((USOPEN - THRITYMINPRE))).do(us_30min_preopen)
    schedule.every().day.at(fixtime(USOPEN)).do(us_markets_open)
    # usclose
    schedule.every().day.at(fixtime((USCLOSE - ONEHOURPRE))).do(us_1hour_preclose)
    schedule.every().day.at(fixtime((USCLOSE - THRITYMINPRE))).do(us_30min_preclose)
    schedule.every().day.at(fixtime(USCLOSE)).do(us_markets_close)

    # asianopen
    schedule.every().day.at(fixtime(ASIANOPEN - ONEHOURPRE)).do(asian_1hour_preopen)
    schedule.every().day.at(fixtime(ASIANOPEN - THRITYMINPRE)).do(asian_30min_preopen)
    schedule.every().day.at(fixtime(ASIANOPEN)).do(asian_markets_open)
    # asianclose
    schedule.every().day.at(fixtime(ASIANCLOSE - ONEHOURPRE)).do(asian_1hour_preclose)
    schedule.every().day.at(fixtime(ASIANCLOSE - THRITYMINPRE)).do(asian_30min_preclose)
    schedule.every().day.at(fixtime(ASIANCLOSE)).do(asian_markets_close)

    # europeanopen
    schedule.every().day.at(fixtime(EUROPEOPEN - ONEHOURPRE)).do(european_1hour_preopen)
    schedule.every().day.at(fixtime(EUROPEOPEN - THRITYMINPRE)).do(european_30min_preopen)
    schedule.every().day.at(fixtime(EUROPEOPEN)).do(european_markets_open)
    # europeanclose
    schedule.every().day.at(fixtime(EUROPECLOSE - ONEHOURPRE)).do(european_1hour_preclose)
    schedule.every().day.at(fixtime(EUROPECLOSE - THRITYMINPRE)
                            ).do(european_30min_preclose)
    schedule.every().day.at(fixtime(EUROPECLOSE)).do(european_markets_close)

    # FOUR HOUR CANDLES
    for i in range(6):
        FOUR_HOUR_CANDLES.append(
            fixtime(FOUR_HOUR_START + timedelta(hours=i * 4)))

    for candle in FOUR_HOUR_CANDLES:
        schedule.every().day.at(candle).do(four_hour_close)

    while True:
        schedule.run_pending()
        sleep(1)
